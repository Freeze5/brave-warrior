﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {
    [SerializeField]
    private float _throwPower;

    public float GetThrowPower { get { return _throwPower; } }

	
}
