﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    private Animator _animator;
    [SerializeField]
    private List<Image> _hearts;
    [SerializeField]
    private int _maxHealth;
    private int _currentHP;
    [SerializeField]
    private Sword _sword;

	
	void Start () {
        _animator = GetComponent<Animator>();
        _currentHP = _maxHealth;
        GetHP();
	}


    private void GetHP()
    {
        for (int i = 0; i < _hearts.Count; i++)
        {
            _hearts[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < _currentHP ; i++)
        {
            _hearts[i].gameObject.SetActive(true);
        }
    }

	void Update () {
        Movement();
        if(_currentHP > _maxHealth)
        {
            _currentHP = _maxHealth;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }
	}


    void Attack()
    {
        var newSword = Instantiate(_sword);
        newSword.transform.position = transform.position;
        Rigidbody2D newSwordRgb = newSword.GetComponent<Rigidbody2D>();
        //swordRotation
        int swordDirection = _animator.GetInteger("direction");
        switch (swordDirection)
        {
            case 0:
                newSword.transform.Rotate(0,0,0);
                newSwordRgb.AddForce(Vector3.up * newSword.GetThrowPower);
                break;
            case 1:
                newSword.transform.Rotate(0, 0, 180);
                newSwordRgb.AddForce(Vector3.up * -newSword.GetThrowPower);
                break;
            case 2:
                newSword.transform.Rotate(0, 0, 90);
                newSwordRgb.AddForce(Vector3.right * -newSword.GetThrowPower);
                break;
            case 3:
                newSword.transform.Rotate(0, 0, -90);
                newSwordRgb.AddForce(Vector3.right * newSword.GetThrowPower);
                break;
        }

    }

    /// <summary>
    /// Player movement
    /// </summary>
    void Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, _speed *Time.deltaTime, 0);
            _animator.SetInteger("direction", 0);
            _animator.speed = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, -_speed * Time.deltaTime, 0);
            _animator.SetInteger("direction", 1);
            _animator.speed = 1;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.Translate( -_speed * Time.deltaTime,0, 0);
            _animator.SetInteger("direction", 2);
            _animator.speed = 1;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(_speed * Time.deltaTime,0, 0);
            _animator.SetInteger("direction", 3);
            _animator.speed = 1;
        }
        else
        {
            _animator.speed = 0;
        }
    }
}
